import copy
import flask
from flask_restful import Resource, reqparse
from werkzeug.datastructures import FileStorage
from enum import Enum
import pandas as pd
from scanner_dgl.processing.utils import merge_all_boxText, out_put_json, image_to_boxes, Process_Invoice, Gen_graph
from scanner_dgl.src.config import Config
import cv2
import torch
import numpy as np
from scanner_dgl.src.model import Invoicemodel
from pathlib import Path
from scanner_dgl.src.utils_dataset import Labels_to_index
import dsflow
path_model = Path(__file__).parent.parent.parent.joinpath('data/model/model_DGL.model')

## loading pretrained mode
obj = torch.load(path_model)
model = Invoicemodel(Config)
model.load_state_dict(obj['model'])
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = model.to(device)
print(model)
print('loading pretrained model from %s' % path_model)
lableToIndex = Labels_to_index
IndexToLable = dict([(value, key) for key, value in lableToIndex.items()])


class Status(Enum):
    SUCCESS = 0
    ERROR = 1
    WARNING = 2

def send_response(status, message, data=None):
    """ All APIs responses have the same structure """
    assert status in Status
    return {"status": status.name.lower(), "message": message, "data": data}

class Upload_image(Resource):
    def __init__(self):
        self._file_parser = reqparse.RequestParser()
        self._file_parser.add_argument("file", type=FileStorage, location="files")
        self.gen_graph = Gen_graph()
        self.process_invoice = Process_Invoice(Config)
    def post(self):
        """ Upload a file that need to be processed """
        file = self._file_parser.parse_args()["file"]
        if not file:
            return send_response(Status.ERROR, "No file was provided"), 400
        npimg = np.fromfile(file, np.uint8)
        img = cv2.imdecode(npimg, cv2.IMREAD_COLOR)
        df = image_to_boxes(img,psm=12,w=Config.resize_Width,h=Config.resize_height)
        self.gen_graph.read(df)
        df = self.gen_graph.connect()
        G,data = self.process_invoice.get(df)

        model.eval()
        preds = model(G.to(device))
        proba, pred = preds.max(1)
        pred = pred.tolist()
        proba = proba.tolist()
        idx_label = [0, 1, 2, 3, 4, 5, 6, 7]
        out_put = out_put_json(pred, proba, data, idx_label, lableToIndex, seuil=0.8)
        # Copy of the text and object arrays
        out = []
        for tag in out_put:
            texts = out_put[tag]["text"]
            texts_boxes = out_put[tag]["box"]
            score = out_put[tag]['score']
            score_box = copy.deepcopy(score)
            texts_copied = copy.deepcopy(texts)
            texts_boxes_copied = copy.deepcopy(texts_boxes)
            texts, boxes, score = merge_all_boxText(texts_boxes_copied, texts_copied, score_box)
            if boxes:
                out_json = [{"label": tag,
                             'ocr_text': texts[idx],
                             'score': score[idx],
                             "box": [{"xmin": box[0], 'ymin': box[1], 'xmax': box[2], 'ymax': box[3]}]} for idx, box in
                            enumerate(boxes)]
                out.append(out_json)

        out = [tagi for tag in out for tagi in tag]
        out = pd.json_normalize(out)
        result = out.to_json(orient="records")

        return flask.Response(response=result, status=200, mimetype='application/json')
        #return send_response(Status.SUCCESS,
        #                     "File '{}' successfully processed".format(id),
        #                     data=out)

    def get(self):
            return {'hello': 'world'}


dsflow.api.add_resource(Upload_image,'/api/image')

#if __name__ == '__main__':
#    app = Flask(__name__)
#    api = Api(app)
#    api.add_resource(Upload_image , "/api")
#    app.run(host='0.0.0.0',port=8800)
