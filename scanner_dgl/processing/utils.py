import torch
import dgl
from scanner_dgl.src.utils_dataset import textSegmentsField, pos_feature, is_edge
import numpy as np
import cv2
import pytesseract
import pandas as pd

class Process_Invoice(object):
    def __init__(self,config):
        self.Width = config.resize_Width
        self.height = config.resize_height

    def get_feature(self, df):
        boxes, transcripts, u, v, graph = [], [], [], [], []
        for idx, row in df.iterrows():
            boxes.append([float(row['xmin']), float(row['ymin']), float(row['xmax']), float(row['ymax'])])
            transcription = str(row['Object'])
            if len(transcription) == 0:
                transcription = ' '
            transcripts.append(transcription)
            ui = idx
            vi = row['v_hor']
            vj = row['v_vert']
            if is_edge(vi):
                u.append(ui)
                v.append(vi)
            if is_edge(vj):
                u.append(ui)
                v.append(vj)
        if 0 not in u and 0 not in v:
            u.append(0)
            v.append(0)
        graph.append(u)
        graph.append(v)
        return boxes, transcripts, torch.LongTensor(graph)
    def get(self, data):
        boxes, transcripts, graph = self.get_feature(data)
        texts, texts_len, mask = textSegmentsField(transcripts)
        x, for_embeddings = pos_feature(boxes,self.Width, self.height)
        G = dgl.DGLGraph((graph[0], graph[1]))
        num_node = x.__len__()
        if G.number_of_nodes() != num_node:
            G.add_nodes(num_node - G.number_of_nodes())
        G.ndata['x'] = x
        G.ndata['for_embeddings'] = for_embeddings
        G.ndata['xtext'] = texts
        G.ndata['mask'] = mask
        G.ndata['texts_len'] = texts_len
        return G,data




def merge_boxes(box1, box2):
    return [min(box1[0], box2[0]),
         min(box1[1], box2[1]),
         max(box1[2], box2[2]),
         max(box1[3], box2[3])]

def calc_sim(box1, box2):
    # box1: ymin, xmin, ymax, xmax
    # box2: ymin, xmin, ymax, xmax
    box1_xmin, box1_ymin, box1_xmax, box1_ymax = box1
    box2_xmin, box2_ymin, box2_xmax, box2_ymax = box2
    x_dist = min(abs(box1_xmin-box2_xmin), abs(box1_xmin-box2_xmax), abs(box1_xmax-box2_xmin), abs(box1_xmax-box2_xmax))
    y_dist = min(abs(box1_ymin-box2_ymin), abs(box1_ymin-box2_ymax), abs(box1_ymax-box2_ymin), abs(box1_ymax-box2_ymax))
    dist = x_dist + y_dist
    return dist
#Distance definition  between text to be merge
dist_limit = 100

#Principal algorithm for merge text
def merge_algo(texts, texts_boxes,score_box):
    for i, (text_1, text_box_1,score1) in enumerate(zip(texts, texts_boxes,score_box)):
        for j, (text_2, text_box_2,score2) in enumerate(zip(texts, texts_boxes,score_box)):
            if j <= i:
                continue
            # Create a new box if a distances is less than disctance limit defined
            if calc_sim(text_box_1, text_box_2) < dist_limit:
            # Create a new box
                new_box = merge_boxes(text_box_1, text_box_2)
             # Create a new text string
                new_text = text_1 + ' ' + text_2
                new_score =(score1+score2)/2
                texts[i] = new_text
                score_box[i] = new_score
                #delete previous text
                del texts[j]
                del score_box[j]
                texts_boxes[i] = new_box
                #delete previous text boxes
                del texts_boxes[j]
                #return a new boxes and new text string that are close
                return True, texts, texts_boxes,score_box

    return False, texts, texts_boxes,score_box

#Merge full text
def merge_all_boxText(boxes,texts,score_box):
    need_to_merge = True
    while need_to_merge:
        need_to_merge, texts_copied, texts_boxes_copied,score_box = merge_algo(texts, boxes,score_box)
    return texts_copied,texts_boxes_copied,score_box

def out_put_json(pred, proba, data, idx_label, lableToIndex, seuil=0.9):

    out_put = {item: {"box": [], "text": [], "score": []} for item in lableToIndex}
    IndexToLable = dict([(value, key) for key, value in lableToIndex.items()])
    for idx, value in enumerate(pred):
        if value in idx_label:
            prob = np.exp(proba[idx])
            if prob > seuil:
                row = data.loc[idx]
                x1 = row['xmin']
                y1 = row['ymin']
                x2 = row['xmax']
                y2 = row['ymax']
                label = IndexToLable[value]
                text = row['Object']
                out_put[label]["box"].append([x1, y1, x2, y2])
                out_put[label]["text"].append(text)
                out_put[label]["score"].append(prob)
    return out_put


def resize_img(img,img_size_x,img_size_y,threshold=0):
    kernel = np.ones((1, 1), np.uint8)
    if not img is None:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.dilate(img,kernel,iterations = 1)
        img = cv2.resize(img, (img_size_x,img_size_y), interpolation = cv2.INTER_AREA)
    return img

def image_to_boxes(img,psm,w,h):
    img = resize_img(img, w, h)
    config = (r'--oem 1 --psm {}'.format(psm))
    boxes = pytesseract.image_to_data(img,lang='fra',config=config)
    xmin = []
    ymin = []
    xmax = []
    ymax = []
    Object = []
    for x, b in enumerate(boxes.splitlines()):
        if x != 0:
            b = b.split()
            if len(b) == 12:
                if int(b[10])>20:
                    x, y, w, h = int(b[6]), int(b[7]), int(b[8]), int(b[9])
                    xmin.append(x)
                    ymin.append(y)
                    xmax.append(x + w)
                    ymax.append(y + h)
                    Object.append(b[11])
    data = {'xmin': xmin, 'ymin': ymin, 'xmax': xmax, 'ymax': ymax, 'Object': Object}
    df = pd.DataFrame(data)
    return df

v_overlap = 0.4
v_margin = 10
h_overlap = 0.5


def overlap(x1, x2, y1, y2):
    res = x2 - x1 + y2 - y1 - (max(y2, x2) - min(y1, x1))
    return max(res, 0)

def check_vertical_edge(pos1, pos2):
    a_x1, a_y1, a_x2, a_y2 = pos1
    b_x1, b_y1, b_x2, b_y2 = pos2
    if b_y1 > a_y1 and overlap(a_x1, a_x2, b_x1, b_x2) > min(a_x2 - a_x1, b_x2 - b_x1) * v_overlap:
        d = b_y1 - a_y2
        if d < max(a_y2 - a_y1, b_y2 - b_y1) * v_margin:
            return d
    return None


def check_horizontal_edge(pos1, pos2):
    a_x1, a_y1, a_x2, a_y2 = pos1
    b_x1, b_y1, b_x2, b_y2 = pos2
    if b_x1 > a_x1 and overlap(a_y1, a_y2, b_y1, b_y2) > min(a_y2 - a_y1, b_y2 - b_y1) * h_overlap:
        return b_x1 - a_x2
    return None

class Gen_graph(object):
    def __init__(self):
        self.df = None
    def read(self, object_map):
        '''
            Function to ensure the data is in correct format and saves the
            dataframe
            Args:
                object_map: pd.DataFrame, having coordinates of bounding boxes,
                                          text object
            Returns:
                None
        '''

        assert type(object_map) == pd.DataFrame, f'object_map should be of type \
            {pd.DataFrame}. Received {type(object_map)}'
        assert 'xmin' in object_map.columns, '"xmin" not in object map'
        assert 'xmax' in object_map.columns, '"xmax" not in object map'
        assert 'ymin' in object_map.columns, '"ymin" not in object map'
        assert 'ymax' in object_map.columns, '"ymax" not in object map'
        assert 'Object' in object_map.columns, '"Object" column not in object map'
        self.df = object_map
        return

    def connect(self):
        '''
            This method implements the logic to generate a graph based on
            visibility. If a horizontal/vertical line can be drawn from one
            node to another, the two nodes are connected.
            Args:

        '''
        df = self.df
        # check if object map was successfully read by .read() method
        try:
            if len(df) == 0:
                return
        except:
            return
        v_vert = []
        v_hor = []
        for ui, u in df.iterrows():
            vj, min_dv = -1, np.Inf
            hj,min_dh = -1, np.Inf
            posu = (u['xmin'],u['ymin'],u['xmax'],u['ymax'])
            ################ iterate over destination objects #################
            for vi, v in df.iterrows():
                if not ui == vj:
                    posv = (v['xmin'], v['ymin'], v['xmax'], v['ymax'])
                    d = check_vertical_edge(posu, posv)
                    if d is not None and d < min_dv:
                        min_dv, vj = d, vi
                    d = check_horizontal_edge(posu, posv)
                    if d is not None and d < min_dh:
                        min_dh, hj = d, vi
            v_vert.append(vj)
            v_hor.append(hj)
        df['v_hor']= v_hor
        df ['v_vert'] = v_vert
        return df






