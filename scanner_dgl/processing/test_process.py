import copy
import dgl
from mlflow.pyfunc import load_model
import pandas as pd
import torch
from pathlib import Path
from scanner_dgl.processing.utils import out_put_json, merge_all_boxText
from scanner_dgl.processing.process import Process_scanner_dgl

## loading pretrained mode
obj = load_model('/BigApps/DS/Indus/Models/mlruns/30/730c599738364ab6a456a2c491ce04c9/artifacts/model')
config = obj['args']

if __name__ == "__main__":
    name_csv = '0319%20-%202019-03-31-systema%20services-75600001-1.csv'
    path_demo = Path(config.root_data) / 'graph' / 'test' / 'connections' / name_csv
    df = pd.read_csv(path_demo.as_posix(),
                     usecols=['xmin', 'ymin', 'xmax', 'ymax', 'Object', 'v_hor', 'v_vert'],
                     dtype={'xmin': int, 'ymin': int, 'xmax': int, 'ymax': int, 'Object': str,
                            'v_hor': int, 'v_vert': int})
    result = Process_scanner_dgl.score(obj,df)

    print(result)











