import datetime
from dsflow.inference.processing.abstracts import Scoringfactory
import copy
import torch
from scanner_dgl.processing.example import example
from scanner_dgl.src.model import Invoicemodel
import pandas as pd
from scanner_dgl.processing.utils import out_put_json, merge_all_boxText, Process_Invoice
from scanner_dgl.src.config import Config
from scanner_dgl.src.utils_dataset import Labels_to_index

config = Config()
process_invoice = Process_Invoice(config)
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model_dgl = Invoicemodel(config).to(device)


lableToIndex = Labels_to_index
IndexToLable = dict([(value, key) for key, value in lableToIndex.items()])
idx_label = [0, 1, 2, 3, 4, 5, 6, 7]

class Process_scanner_dgl(Scoringfactory):
    @classmethod
    def score(self, loaded_model, df_to_score, extra_args=None):
        """
            from train phase we saved model as :
           dict_model = dict()
           dict_model['missing_values'] = dict_NA
           dict_model['model_features'] = list(rf_var_select)
           dict_model['model_rf'] = rf_final
        """
        convert_dict = {'xmin': int,
                        'ymin': int,
                        'xmax': int,
                        'ymax': int,
                        'Object': str,
                        'v_hor': int,
                        'v_vert': int
                        }
        df_to_score = df_to_score.astype(convert_dict)
        model_dgl.load_state_dict(loaded_model['model'])
        model_dgl.eval()

        G, data = process_invoice.get(df_to_score)
        preds = model_dgl(G)
        proba, pred = preds.max(1)
        pred = pred.tolist()
        proba = proba.tolist()
        out_put = out_put_json(pred, proba, data, idx_label, lableToIndex, seuil=0.1)
        # Copy of the text and object arrays
        out = []
        for tag in out_put:
            texts = out_put[tag]["text"]
            texts_boxes = out_put[tag]["box"]
            score = out_put[tag]['score']
            score_box = copy.deepcopy(score)
            texts_copied = copy.deepcopy(texts)
            texts_boxes_copied = copy.deepcopy(texts_boxes)
            texts, boxes, score = merge_all_boxText(texts_boxes_copied, texts_copied, score_box)
            if boxes:
                out_json = [{"label": tag,
                             'ocr_text': texts[idx],
                             'score': score[idx],
                             "box": [{"xmin": box[0], 'ymin': box[1], 'xmax': box[2], 'ymax': box[3]}]} for idx, box in
                            enumerate(boxes)]
                out.append(out_json)

        out = [tagi for tag in out for tagi in tag]
        out = pd.json_normalize(out)
        return out
    @classmethod
    def get_schema(cls):
        schema = {"fields": [{"metadata": {}, "name": "label", "nullable": True, "type": "string"},
                             {"metadata": {}, "name": "box", "nullable": True, "type": "string"},
                             {"metadata": {}, "name": "score", "nullable": True, "type": "float"},
                             ]}

        return schema

    @classmethod
    def get_score_name(cls):
        return "scanner_dgl"

    @classmethod
    def get_example(self):
        # example for score client with api
        return example

    @classmethod
    def get_score_date(cls):
        return datetime.date.today().strftime("%d-%m-%Y")
