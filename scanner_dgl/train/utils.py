from __future__ import print_function
import torch.utils.data
import torch.nn as nn
from torch.utils.data import DataLoader
import torch.nn.init as init
from torch.autograd import Variable
from sklearn.metrics import classification_report
import torch

from scanner_dgl.src.utils_dataset import Labels_to_index


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def weight_init(m):
    '''
    Usage:
        model = Model()
        model.apply(weight_init)
    '''
    if isinstance(m, nn.Conv1d):
        init.normal_(m.weight.data)
        if m.bias is not None:
            init.normal_(m.bias.data)
    elif isinstance(m, nn.Conv2d):
        init.xavier_normal_(m.weight.data)
        if m.bias is not None:
            init.normal_(m.bias.data)
    elif isinstance(m, nn.Conv3d):
        init.xavier_normal_(m.weight.data)
        if m.bias is not None:
            init.normal_(m.bias.data)
    elif isinstance(m, nn.ConvTranspose1d):
        init.normal_(m.weight.data)
        if m.bias is not None:
            init.normal_(m.bias.data)
    elif isinstance(m, nn.ConvTranspose2d):
        init.xavier_normal_(m.weight.data)
        if m.bias is not None:
            init.normal_(m.bias.data)
    elif isinstance(m, nn.ConvTranspose3d):
        init.xavier_normal_(m.weight.data)
        if m.bias is not None:
            init.normal_(m.bias.data)
    elif isinstance(m, nn.BatchNorm1d):
        init.normal_(m.weight.data, mean=1, std=0.02)
        init.constant_(m.bias.data, 0)
    elif isinstance(m, nn.BatchNorm2d):
        init.normal_(m.weight.data, mean=1, std=0.02)
        init.constant_(m.bias.data, 0)
    elif isinstance(m, nn.BatchNorm3d):
        init.normal_(m.weight.data, mean=1, std=0.02)
        init.constant_(m.bias.data, 0)
    elif isinstance(m, nn.Linear):
        init.xavier_normal_(m.weight.data)
        init.normal_(m.bias.data)
    elif isinstance(m, nn.LSTM):
        for param in m.parameters():
            if len(param.shape) >= 2:
                init.orthogonal_(param.data)
            else:
                init.normal_(param.data)
    elif isinstance(m, nn.LSTMCell):
        for param in m.parameters():
            if len(param.shape) >= 2:
                init.orthogonal_(param.data)
            else:
                init.normal_(param.data)
    elif isinstance(m, nn.GRU):
        for param in m.parameters():
            if len(param.shape) >= 2:
                init.orthogonal_(param.data)
            else:
                init.normal_(param.data)
    elif isinstance(m, nn.GRUCell):
        for param in m.parameters():
            if len(param.shape) >= 2:
                init.orthogonal_(param.data)
            else:
                init.normal_(param.data)

def trainBatch(train_iter, model,criterion, optimizer):
    classes = Labels_to_index.values()
    data = train_iter.next()
    optimizer.zero_grad()
    preds = model(data.to(device))

    cost =  criterion(preds.view(-1, len(classes)),data.ndata['y'].view(-1))
    cost.backward()
    optimizer.step()
    return cost


class averager(object):
    """Compute average for `torch.Variable` and `torch.Tensor`. """

    def __init__(self):
        self.reset()

    def add(self, v):
        if isinstance(v, Variable):
            count = v.data.numel()
            v = v.data.sum()
        elif isinstance(v, torch.Tensor):
            count = v.numel()
            v = v.sum()

        self.n_count += count
        self.sum += v

    def reset(self):
        self.n_count = 0
        self.sum = 0

    def val(self):
        res = 0
        if self.n_count != 0:
            res = self.sum / float(self.n_count)
        return res

def val(net, dataset, criterion, max_iter=100):
    print('Start val')
    all_preds = []
    all_y = []
    for p in net.parameters():
        p.requires_grad = False
    net.eval()
    data_loader = DataLoader(dataset, batch_size=5, shuffle=True,collate_fn=dataset.collate_fn)
    val_iter = iter(data_loader)
    max_iter = len(data_loader)
    for i in range(max_iter):
        data = val_iter.next()
        i += 1
        preds = net(data.to(device))
        y = data.ndata['y']
        _, pred = preds.max(1)
        label = y.detach().cpu().numpy()
        preds = pred.detach().cpu().numpy()
        all_preds.extend(preds)
        all_y.extend(label)
    report = classification_report(all_y,all_preds,target_names= Labels_to_index.keys(),output_dict=True)
    report2 = classification_report(all_y,all_preds,target_names= Labels_to_index.keys())#,output_dict=True)
    return report,report2

