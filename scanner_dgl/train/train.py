import dsflow
import random
import torch
import numpy as np
from scanner_dgl.src.config import Config
from scanner_dgl.src.dataset import InvoiceDataset
from torch.utils.data import DataLoader
from scanner_dgl.src.model import Invoicemodel
from scanner_dgl.train.utils import weight_init,averager,trainBatch,val
from torch import nn
import  torch.optim as optim
import os

config = Config()
train_dataset = InvoiceDataset(path_data=config.root_data, split='train')
validation_dataset = InvoiceDataset(path_data=config.root_data, split='test')
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
model = Invoicemodel(config).to(device)
model.apply(weight_init)
print("samples:", len(train_dataset), len(validation_dataset))

train_loader = DataLoader(train_dataset, batch_size=config.batch_size, shuffle=True,
                          collate_fn=train_dataset.collate_fn)
# criterion = torch.nn.NLLLoss()
weight = torch.tensor([0.12, 0.11147322931505445, 0.21493640622423674, 0.046616077713568224, 0.1441541359013957,
                       0.04130538531581995, 0.19941322133026407, 0.12911647424261702, 0.0008])

criterion = nn.CrossEntropyLoss(weight.to(device))
optimizer = optim.Adam(model.parameters(), lr=config.lr)  # ,weight_decay=5e-4)
scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=50, gamma=0.1)



def train():
    print("----------------Start train-----------------------")
    # loss averager
    loss_avg = averager()
    best_acc = -1
    for epoch in range(config.niter):
        train_iter = iter(train_loader)
        i = 0
        print('epoch', epoch, ' dataset size:', len(train_loader))

        while i < len(train_loader):
            for p in model.parameters():
                p.requires_grad = True
            model.train()
            cost = trainBatch(train_iter, model, criterion, optimizer)
            loss_avg.add(cost)
            i += 1
            if i % 1 == 0:
                print('[%d/%d][%d/%d] Loss: %f' %
                      (epoch, config.niter, i, len(train_loader), loss_avg.val()))
        loss_avg.reset()
        if epoch % 1 == 0:
            print("val---------------Test-----------------:")
            report, raport2 = val(model, validation_dataset, criterion)
            print(raport2)
            f1_score = report['macro avg']['f1-score']
            recall = report['macro avg']['recall']
            precision = report['macro avg']['precision']

            if f1_score >= best_acc:
                best_f1_score = f1_score
                best_recall = recall
                best_precisiion = precision
                obj = {'args': config, 'model': model.state_dict()}
                torch.save(obj, config.save_path.as_posix() + '.model')
            best_acc = max(f1_score, best_acc)
            print("val---------- train -----------------:")
            _, report = val(model, train_dataset, criterion)
            print(report)
        scheduler.step()
        print('Epoch-{0} lr: {1}'.format(epoch, optimizer.param_groups[0]['lr']))
    dsflow.log_metric('f1_score', best_f1_score)
    dsflow.log_metric('recall', best_recall)
    dsflow.log_metric('precision', best_precisiion)
    dsflow.log_param("lr", config.lr)
    dsflow.log_param('batch_size', config.batch_size)
    dsflow.log_param('niter', config.niter)
    dsflow.sklearn.log_model(obj, "model", serialization_format='pickle')
if __name__ == '__main__':
    os.path.abspath(os.curdir)
    dsflow.train(name_experiment='scanner_dgl', train_method=train)