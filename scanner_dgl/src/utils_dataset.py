from typing import *
from collections import Counter
from torchtext.vocab import Vocab
from pathlib import Path
from torchtext.data import Field
from string import ascii_letters,punctuation, digits
import torch
import pandas as pd

Labels_to_index ={'TOTAL': 0,
                  'DATE': 1,
                  'NUMBER': 2,
                  'COMPANY_ADDRESS': 3,
                  'COMPANY_NAME': 4,
                  'CLIENT_ADDRESS': 5,
                'CLIENT_NAME': 6,
                'LOGO': 7,
                'O': 8}

alphabet = digits+ascii_letters+"éè€àû° "+punctuation
MAX_TRANSCRIPT_LEN = 10
class ClassVocab(Vocab):

    def __init__(self, classes, specials=['<pad>', '<unk>'], **kwargs):
        '''
        convert key to index(stoi), and get key string by index(itos)
        :param classes: list or str, key string or entity list
        :param specials: list, speical tokens except <unk> (default: {['<pad>', '<unk>']})
        :param kwargs:
        '''
        cls_list = None
        if isinstance(classes, str):
            cls_list = list(classes)
        if isinstance(classes, Path):
            p = Path(classes)
            if not p.exists():
                raise RuntimeError('Key file is not found')
            with p.open(encoding='utf8') as f:
                classes = f.read()
                classes = classes.strip()
                cls_list = list(classes)
        elif isinstance(classes, list):
            cls_list = classes
        c = Counter(cls_list)
        self.special_count = len(specials)
        super().__init__(c, specials=specials, **kwargs)

keys_vocab_cls = ClassVocab(alphabet, specials_first=False)

# text string label converter
TextSegmentsField = Field(sequential=True, use_vocab=True, include_lengths=True, batch_first=True,fix_length=MAX_TRANSCRIPT_LEN)
TextSegmentsField.vocab = keys_vocab_cls

def textSegmentsField(text_segments):
    boxes_num = len(text_segments)
    mask = torch.zeros(boxes_num,MAX_TRANSCRIPT_LEN)
    text_segments = [list(trans) for trans in text_segments]
    texts, texts_len = TextSegmentsField.process(text_segments)
    for i in range(boxes_num):
        mask[i, :texts_len[i]] = 1
    return texts,texts_len,mask


Entities_list = Labels_to_index.keys()

def tag2io_label(annotation_box_types: List[str]) -> List[int]:
    tags = []
    for entity_type in annotation_box_types:
        if entity_type in Entities_list:
            tags.append(Labels_to_index[entity_type])
        else:
            tags.append(Labels_to_index['O'])
    return torch.LongTensor(tags)



def is_edge(v: int):
    if v!=-1:
        return True
    return False

def read_gt_file_with_box_entity_type(filepath: str):
    # read boxes, transcripts, and entity types of boxes in one documents from boxes_and_transcripts file
    # m index,x1,y1,x2,y2,transcript,type from boxes_and_transcripts csv file
    # data format as  points, transcription, entity_type ,graph

    df = pd.read_csv(filepath,
        usecols=['xmin', 'ymin', 'xmax', 'ymax', 'Object', 'label', 'v_hor', 'v_vert'],

        dtype={'xmin': int, 'ymin': int, 'xmax': int, 'ymax': int, 'Object': str, 'label': str, 'v_hor': int, 'v_vert': int})
    boxes,transcripts,box_entity_types,u,v,graph= [],[],[],[],[],[]
    for idx,row in df.iterrows():
        boxes.append([float(row['xmin']),float(row['ymin']),float(row['xmax']),float(row['ymax'])])
        box_entity_types.append(str(row['label']))
        transcription = str(row['Object'])
        if len(transcription) == 0:
            transcription = ' '
        transcripts.append(transcription)
        ui = idx
        vi = row['v_hor']
        vj = row['v_vert']
        if is_edge(vi):
            u.append(ui)
            v.append(vi)
        if is_edge(vj):
            u.append(ui)
            v.append(vj)
    if 0 not in u and 0 not in v:
        u.append(0)
        v.append(0)
    graph.append(u)
    graph.append(v)
    return boxes,transcripts,box_entity_types, torch.LongTensor(graph)


def pos_feature(boxes,Width,Height):
    pos,for_embeddings = [],[]
    for  box in boxes:
        x1 = box[0] / Width
        x2 = box[2] / Width
        x3 = box[1] / Height
        x4 = box[3] / Height
        x5 = (x1 + x2) * 0.5
        x6 = (x3 + x4) * 0.5
        #x7 = abs(x2 - x1)
        #x8 = abs(x4 - x3)
        xmin = int(box[0])
        ymin = int(box[1])
        xmax = int(box[2])
        ymax = int(box[3])
        w = int(abs(xmax-xmin))
        h = int(abs(ymax-ymin))
        for_embeddings.append([xmin,ymin,xmax,ymax,w,h])
        pos.append([x5, x6])
    return torch.FloatTensor(pos),torch.LongTensor(for_embeddings)

