import random
import warnings
import dgl
from torch.utils.data import DataLoader
from pathlib import Path
from scanner_dgl.src.config import Config
from typing import *
from scanner_dgl.src.utils_dataset import read_gt_file_with_box_entity_type, textSegmentsField, pos_feature, tag2io_label

def document(boxes_and_transcripts_file : Path,
                 resized_image_size: Tuple[int, int] = (1200, 1500)):
        boxes, transcripts, box_entity_types,graph= read_gt_file_with_box_entity_type(boxes_and_transcripts_file.as_posix())
        texts, texts_len, mask = textSegmentsField(transcripts)
        x,for_embeddings = pos_feature(boxes,resized_image_size[0],resized_image_size[1])
        y = tag2io_label(box_entity_types)
        G = dgl.DGLGraph((graph[0],graph[1]))
        num_node = y.__len__()
        if G.number_of_nodes() != num_node:
            G.add_nodes(num_node - G.number_of_nodes())
        G.ndata['x'] = x
        G.ndata['y'] = y
        G.ndata['for_embeddings'] = for_embeddings
        G.ndata['xtext'] = texts
        G.ndata['mask'] = mask
        G.ndata['texts_len'] = texts_len
        return G

class InvoiceDataset(object):
    def __init__(self, path_data: str = None,
                 split: str ='train',
                 resized_image_size: Tuple[int, int] = (1200, 1500),
                 ignore_error: bool = False
                 ):
        super().__init__()
        self.root_csv = Path(path_data) / 'graph' / split / 'connections'
        self.list_csv = [p for p in self.root_csv.glob('*.csv')]
        self.ignore_error = ignore_error
        assert resized_image_size and len(resized_image_size) == 2, 'resized image size not be set.'
        self.resized_image_size = resized_image_size  # (w, h)

    def __len__(self):
        return len(self.list_csv)

    def is_file_exist(self, boxes_and_transcripts_file):
        if not boxes_and_transcripts_file.exists() :
            if self.ignore_error :
                warnings.warn('{} is not exist. get a new one.'.format(boxes_and_transcripts_file))
                new_item = random.randint(0, len(self) - 1)
                return self.__getitem__(new_item)
            else:
                raise RuntimeError('Sample: {} not exist.'.format(boxes_and_transcripts_file.stem))

        return boxes_and_transcripts_file
    def __getitem__(self, index):
        boxes_and_transcripts_file = self.list_csv[index]
        boxes_and_transcripts_file = self.is_file_exist(boxes_and_transcripts_file)
        doc = document(boxes_and_transcripts_file,self.resized_image_size)
        return doc
    def collate_fn(self,samples):
        batched_graph = dgl.batch(samples)
        return batched_graph

if __name__ == "__main__":

    config = Config()
    ds = InvoiceDataset(config.root_data,split='test')
    test_loader = DataLoader(ds, batch_size=1,shuffle=False,collate_fn=ds.collate_fn)
    for data in test_loader:
        print(data )
