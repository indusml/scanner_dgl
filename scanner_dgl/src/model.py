from __future__ import print_function
from dgl.nn.pytorch import GraphConv
from scanner_dgl.src.config import Config
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
from torch.utils.data import DataLoader
from scanner_dgl.src.dataset import InvoiceDataset
import torch
from torch import nn
from typing import *
import torch.nn.functional as F
from scanner_dgl.src.utils_dataset import  keys_vocab_cls, Entities_list


class LSTM(nn.Module):
    def __init__(self,embed_size,hidden_size,hidden_layers,dropout_keep,bidirectional):
        super(LSTM, self).__init__()
        self.lstm = nn.LSTM(input_size=embed_size,
                           hidden_size=hidden_size,
                           num_layers=hidden_layers,
                           dropout=dropout_keep,
                           bidirectional=bidirectional,batch_first=True)

    @staticmethod
    def sort_tensor(x: torch.Tensor, length: torch.Tensor, h_0: torch.Tensor = None, c_0: torch.Tensor = None):
        sorted_lenght, sorted_order = torch.sort(length, descending=True)
        _, invert_order = sorted_order.sort(0, descending=False)
        if h_0 is not None:
            h_0 = h_0[:, sorted_order, :]
        if c_0 is not None:
            c_0 = c_0[:, sorted_order, :]
        return x[sorted_order], sorted_lenght, invert_order, h_0, c_0


    def _aggregate_avg_pooling(self, input, text_mask):
        '''
        Apply mean pooling over time (text length), (B*N, T, D) -> (B*N, D)
        :param input: (B*N, T, D)
        :param text_mask: (B*N, T)
        :return: (B*N, D)
        '''
        # filter out padding value, (B*N, T, D)
        input = input * text_mask.detach().unsqueeze(2).float()
        # (B*N, D)
        sum_out = torch.sum(input, dim=1)
        # (B*N, )
        text_len = text_mask.float().sum(dim=1)
        # (B*N, D)
        text_len = text_len.unsqueeze(1).expand_as(sum_out)
        text_len = text_len + text_len.eq(0).float()  # avoid divide zero denominator
        # (B*N, D)
        mean_out = sum_out.div(text_len)
        return mean_out

    def forward(self, x_seq: torch.Tensor,
                    lenghts: torch.Tensor,
                    mask: torch.Tensor,
                    initial: Tuple[torch.Tensor, torch.Tensor]):

        # B*N, T, hidden_size
        x_seq, sorted_lengths, invert_order, h_0, c_0 = self.sort_tensor(x_seq, lenghts, initial[0], initial[0])
        packed_x = pack_padded_sequence(x_seq, lengths=sorted_lengths, batch_first=True)

        output, _ = self.lstm(packed_x)
        output, _ = pad_packed_sequence(output, batch_first=True)
        output = output[invert_order]
        output = self._aggregate_avg_pooling(output,mask)
        return output

class Embeddings(nn.Module):
    def __init__(self, vocab_size=len(keys_vocab_cls),hidden_size=256,dropout_keep=0.2,w=1200,h=1500):
        super(Embeddings, self).__init__()
        hidden_layers = 2
        bidirectional = True
        embed_size = hidden_size*(1+bidirectional)
        self.word_embeddings = nn.Embedding(vocab_size, embed_size)
        self.rnn = LSTM(embed_size,
                        int(hidden_size/2),
                        hidden_layers,
                        dropout_keep,
                        bidirectional)

        self.x_position_embeddings = nn.Embedding(w, hidden_size)
        self.y_position_embeddings = nn.Embedding(h, hidden_size)
        self.h_position_embeddings = nn.Embedding(h, hidden_size)
        self.w_position_embeddings = nn.Embedding(w,hidden_size)
        self.LayerNorm = nn.LayerNorm(hidden_size)
        self.dropout = nn.Dropout(dropout_keep)

    def forward(self,xtext,mask,texts_len,bbox):
        words_embeddings = self.word_embeddings(xtext)
        words_embeddings = self.rnn(words_embeddings,texts_len,mask,(None,None))
        bbox = bbox.unsqueeze(1)
        left_position_embeddings = self.x_position_embeddings(bbox[:,:,0])
        upper_position_embeddings = self.y_position_embeddings(bbox[:,:, 1])
        right_position_embeddings = self.x_position_embeddings(bbox[:,:, 2])
        lower_position_embeddings = self.y_position_embeddings(bbox[:,:, 3])
        h_position_embeddings = self.h_position_embeddings(bbox[:,:, 5])
        w_position_embeddings = self.w_position_embeddings(bbox[:,:, 4])
        left_position_embeddings = left_position_embeddings.squeeze(1)
        upper_position_embeddings = upper_position_embeddings.squeeze(1)
        right_position_embeddings = right_position_embeddings.squeeze(1)
        lower_position_embeddings = lower_position_embeddings.squeeze(1)
        h_position_embeddings = h_position_embeddings.squeeze(1)
        w_position_embeddings = w_position_embeddings.squeeze(1)

        embeddings = (
            words_embeddings
            + left_position_embeddings
            + upper_position_embeddings
            + right_position_embeddings
            + lower_position_embeddings
            + h_position_embeddings
            + w_position_embeddings)
        embeddings = self.LayerNorm(embeddings)
        embeddings = self.dropout(embeddings)
        return embeddings



class Invoicemodel(nn.Module):
    def __init__(self,config):
        super(Invoicemodel, self).__init__()
        self.vocab_size= len(keys_vocab_cls)
        self.embed_size = config.embed_size
        self.hidden_layers = config.hidden_layers
        self.hidden_size = config.hidden_size
        self.bidirectional = config.bidirectional
        self.dropout_keep = config.dropout_keep
        self.out_dim = len(Entities_list)
        self.embeds = Embeddings(vocab_size=self.vocab_size,hidden_size=self.embed_size,
                                 dropout_keep=self.dropout_keep,w=config.resize_Width+1 ,h=config.resize_height+1)

        self.decoder_lstm= nn.LSTM(input_size=self.embed_size,
                           hidden_size=self.hidden_size,
                           num_layers=self.hidden_layers,
                           bidirectional=self.bidirectional,batch_first=True)
        #self.edge_attr = torch.nn.Linear(7,1)
        self.conv1 = GraphConv(self.embed_size ,128) # text
        self.conv2 = GraphConv(128,256)
        self.conv3 = GraphConv(256,self.embed_size)
        self.relu = nn.ReLU()
        self.lin_final1 = torch.nn.Linear(300, 300)
        self.lin_final = torch.nn.Linear(300,self.out_dim)
    def forward(self, data):
        x,xtext,mask,texts_len,bbox= data.ndata['x'],data.ndata['xtext'],\
        data.ndata['mask'], data.ndata['texts_len'],data.ndata['for_embeddings']
        xtext = self.embeds(xtext,mask,texts_len,bbox)
        pos_text = F.dropout(self.relu(self.conv1(data,xtext)), p=0.1, training=self.training)
        pos_text = F.dropout(self.relu(self.conv2(data,pos_text)), p=0.1, training=self.training)
        pos_text = F.dropout(self.relu(self.conv3(data,pos_text)), p=0.1, training=self.training)

        pos_text = pos_text+xtext
        pos_text = pos_text.unsqueeze(0)
        pos_text,_ = self.decoder_lstm(pos_text)
        pos_text =  pos_text.squeeze(0)

        pos_text = self.lin_final1(pos_text)
        pos_text = F.dropout(F.relu(pos_text),p=0.3,training=self.training)
        pos_text = self.lin_final(pos_text)
        return F.log_softmax(pos_text, dim=1)

if __name__ == "__main__":
    config = Config()
    train_dataset = InvoiceDataset(config.root_data ,split='test')
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model = Invoicemodel(config).to(device)
    print("samples:", len(train_dataset))
    train_loader = DataLoader(train_dataset, batch_size=3, shuffle=True,collate_fn=train_dataset.collate_fn)
    train_iter = iter(train_loader)
    data = train_iter.next()
    preds = model(data.to(device))
    print(preds.size())
