import os
from pathlib import Path

class Config(object):
    root_data = Path(os.path.dirname(os.path.abspath(__file__))) / '../data'
    save_path = Path(os.path.dirname(os.path.abspath(__file__))) / '../data'/'model'/'model_DGL'
    default_class = 'O'
    ############image############
    resize_Width = 1200
    resize_height = 1500
    ################LSTM########################
    embed_size = 300
    hidden_layers = 2
    hidden_size = 150
    bidirectional = True
    dropout_keep = 0.2
    ##################train#############
    niter = 150
    lr = 0.001
    batch_size = 3